# README #

Tout savoir sur la conception du lavage de voiture à la française
Si l’automobile reste au cœur des dépenses des ménages en hexagone, ce n’est pas un hasard. Cet outil de transport symbolise à la fois l’image du conducteur et son milieu social. Avec un nombre de véhicules en constante augmentation, plus de 30 millions selon les dernières estimations, le recours au lavage auto est incontournable. 
Quand faut-il laver sa voiture
Si l’on s’en tient à des études menées sur le terrain seul, 1 % de foyer reconnaît ne pas procéder au nettoyage de leur voiture. Pour d’autres, la fréquence de lavage mensuel de la voiture atteint les 18 %. Les usagers exerçant une activité salissante comme les artisans font monter la fréquence par mois à 30 %.
De manière générale, au moins 84 % des voitures sont lavés une fois dans l’année, mais ce timing est loin d’être le plus courant et les automobilistes français lavent leur voiture une fois tous les trimestres. 
Le recours aux appareils de haute technologie
Si les disparités sont grandes quant à la fréquence de lavage, les moyens utilisés pour mettre sa voiture au propre sont loin de faire l’unanimité. 78 % des usagers ont recours à une station de lavage ou préfèrent confier leur automobile à un professionnel.
Le lavage de voiture aux rouleaux est l’option la plus sollicitée, ainsi, 38 % des personnes confient leurs véhicules à des entreprises spécialisées en lavage de voiture. Dans la partie sud-ouest du pays, cette solution de lavage remporte les suffrages de 46 % des automobilistes.
L’utilisation du lavage au jet haute pression prend la deuxième avec 30 % des votes. Cette option satisfait parfaitement les personnes qui prennent souvent soin de leur automobile. 40 % des usagers qui procèdent à un lavage mensuel le plébiscitent. Moins utilisé, le lavage à la main en station glane 7 % des voix tandis que la sélection d’un laveur mobile reçoit 3 % des votes.
Lavage à domicile
Ils sont encore au moins un automobiliste sur cinq à procéder au lavage de leur voiture à domicile. Ce qui est une infraction à l’article L35-8 du code de santé publique. Cette réglementation demande que le lavage des eaux provenant du lavage de véhicule subisse un traitement dans un séparateur d’hydrocarbures avant son passage dans le réseau servant à canaliser les eaux usées.
Pourtant, dans les communes rurales, ils sont encore 26 % à procéder au lavage chez eux, le nombre descend à 17 % en province contre 15 % pour la seule région parisienne. Les dégâts de ces procédés sur l’environnement sont donc considérables heureusement, les usagers sont prêts à faire évoluer leurs habitudes.
Un changement de comportement
Pour intégrer les stations de lavage dans leur quotidien, les automobilistes sont nombreux à confier leur voiture à des sociétés de lavage proche de chez eux. Pour 25 % d’entre eux, le prix du programme de lavage est aussi important. Cette réalité est plus grande chez les jeunes conducteurs qui ont entre 18-24 ans. Ils déboursent en effet moins de 6 € pour le nettoyage de leur auto alors que les prix sur le terrain oscillent entre 6 et 15 €.
22 % d’entre eux privilégient plutôt la qualité de lavage, cette tendance est impulsée par les hommes. On notera aussi que de mauvaises conditions météo peuvent empêcher les conducteurs de passer au lavage. Il est enfin essentiel de noter que les trois quarts des usagers sont attirés par le lavage en station si leur entreprise de lavage leur propose une réduction de prix à des moments clés de la journée. 79 % des 18-24 ans y sont très favorable.

https://star-wash.fr/